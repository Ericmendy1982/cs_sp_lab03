
//  main.c
//  
//  Created by Hong on 2020/11/15.


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


int open_file(char *fname){
    if(fopen(fname, "r") == NULL){
		return -1;
	}

    return 0;
}


// define a function to calculate the mean
double cal_mean(float *pointer, int n){
    double sum = 0;
    for(int i=0;i<n;i++){
        sum+=pointer[i];
    }
    double mean=sum/n;
    return mean;
}


// define a function to calculate the standard deviation
// standard_deviation = sqrt((sum((Xi-mean)^2))/N)
double cal_std_dev(float *pointer,int n){
    double sd_sum=0;
    double mean=cal_mean(pointer,n);
    for(int i=0;i<n;i++){
        sd_sum+=pow(pointer[i]-mean,2);
    }
    double std_dev=sqrt(sd_sum/n);
    return std_dev;
}


//define a function to calculate the median
double cal_median(float *cur_arr,int n){
    if(n%2==1)
        return(cur_arr[n/2]);
    else{
        double left=cur_arr[n/2-1];
        double right=cur_arr[n/2];
        double median=((left+right)/2);
        return median;
    }
}


// define a function to sort the array, because n is not big, we apply bubble sort
void swap(float *a, float *b){
    float temp=*a;
    *a=*b;
    *b=temp;
}


// bubble sort
void sort(float *cur_arr,int n){
    int i,j,sort_idx;
    for(i=0;i<n-1;i++){
        sort_idx=i;
        for(j=i+1;j<n;j++){
            if(cur_arr[j]<cur_arr[sort_idx])
                sort_idx=j;
        }
        swap(&cur_arr[sort_idx],&cur_arr[i]);
    }
}






int main(){

int n=20,len_arr=0;
    float *cur_arr=(float *)malloc(n *sizeof(float));
    char file_name[50];
	printf("Please input file name:\n");
	scanf("%s",file_name);
  //  char *file_name=argv[1];

      if (open_file(file_name)==-1){
            printf("error reading file\n");
            return -1;
        }
    FILE *file_pointer;
    file_pointer=fopen(file_name,"r");
    while (!feof(file_pointer)){
        fscanf(file_pointer,"%f\n",cur_arr+len_arr);
        len_arr++;
        if(len_arr==n){          //the current array length is equal to the maximum capacity
            // enlarge the array size twice
            float *new_array=(float *) malloc(n*2*sizeof(float));
            //copy values from the old array to new array
            memcpy(new_array, cur_arr, len_arr*sizeof(float));
            //free memory
            free(cur_arr);
            cur_arr=new_array;
            //increase the size of n
            n=n*2;
        }
    }
    fclose(file_pointer);
    float mean=cal_mean(cur_arr,len_arr);
    float std_dev=cal_std_dev(cur_arr,len_arr);
    printf("Result:\n--------\n");
    printf("%15s %10d\n","Num values:",len_arr);
    printf("%15s %10.3f\n","The mean is:",mean);
    printf("%15s %10.3f\n","The stddev is:",std_dev);
    sort(cur_arr,len_arr);
    float median=cal_median(cur_arr,len_arr);
    printf("%15s %10.3f\n","The median is:",median);
    int Unused=n-len_arr;
    printf("Unused array capacity:  %d\n",Unused);
    
}

